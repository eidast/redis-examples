require "redis"

redis = Redis.new(
          host: ENV["REDIS_HOST"], 
          port: ENV["REDIS_PORT"], 
          db: 15,
          password: ENV["REDIS_PASSWORD"],
          ssl: true)

status = redis.set('foo', 'bar')
foo = redis.get('foo')
puts foo